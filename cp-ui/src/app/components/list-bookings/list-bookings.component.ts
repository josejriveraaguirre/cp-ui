import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/Booking';
import { BookingService } from 'src/app/services/booking.service';

@Component({
  selector: 'app-list-bookings',
  templateUrl: './list-bookings.component.html',
  styleUrls: ['./list-bookings.component.css']
})
export class ListBookingsComponent implements OnInit {

  bookings: Booking[] = [];

  constructor(private bookingSvc: BookingService) { }

  ngOnInit(): void {

    this.listBookings();
    
  } //end of ngOnInit

  listBookings() {
    this.bookingSvc.getBookings().subscribe(
      data => this.bookings = data 
    )
  }

  deletedBooking(id: number) {
    this.bookingSvc.deleteBooking(id).subscribe(
      data => this.listBookings()
    )
  }

} // end of class
