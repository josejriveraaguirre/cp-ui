import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from 'src/app/models/Booking';
import { BookingService } from 'src/app/services/booking.service';

@Component({
  selector: 'app-add-booking',
  templateUrl: './add-booking.component.html',
  styleUrls: ['./add-booking.component.css']
})
export class AddBookingComponent implements OnInit {

  newBooking: Booking = {
    id: 0,

    origEntryDate: "",

    guestFirstName: "",

    guestLastName: "",

    guestEmail: "",

    guestCellphone: "",

    partyOf: 0,

    locationRequested: "",

    arrivalDate: "",

    departureDate: "",

    status: "",

    lastModifiedDate: "",

    reviewedBy: "",

    lastReviewDate: "",
  };

  isEditing: boolean = false;

  constructor(private bookingSvc: BookingService, 
    private router: Router, 
    private activeRoute: ActivatedRoute) {}

  ngOnInit(): void {

    var isIdPresent = this.activeRoute.snapshot.paramMap.has("id");

    if (isIdPresent) {
      const id = this.activeRoute.snapshot.paramMap.get('id')
      this.bookingSvc.viewBooking(Number(id)).subscribe(
        data => this.newBooking = data
      )
      this.isEditing = true;
    }
  } // end of ngOnInit

  savedBooking() {
    this.bookingSvc.saveBooking(this.newBooking).subscribe(
      data => {
        this.router.navigateByUrl("/bookings");
      }
    )
  }

  deletedBooking(id: number) {
    this.bookingSvc.deleteBooking(id).subscribe(
      data => {
        this.router.navigateByUrl("/bookings");
      }
    )
  }

} // end of class
