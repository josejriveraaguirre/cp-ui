import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Booking } from '../models/Booking';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private getUrl: string = "http://localhost:8080/api/v1/bookings";

    constructor(private httpClient: HttpClient) { }

    getBookings(): Observable<Booking[]> {
      return this.httpClient.get<Booking[]>(this.getUrl).pipe(
        map (result => result)
      )
    }

    saveBooking(newBooking: Booking): Observable<Booking> {
      return this.httpClient.post<Booking>(this.getUrl, newBooking);
    }

    viewBooking(id: number): Observable<Booking> {
      return this.httpClient.get<Booking>(`${this.getUrl}/${id}`).pipe(
        map(result => result)
      )
    }

    deleteBooking(id: number): Observable<any> {
      return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: "text"})
    }
    
} // end of class
