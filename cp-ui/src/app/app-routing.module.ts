import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddBookingComponent } from './components/add-booking/add-booking.component';
import { ListBookingsComponent } from './components/list-bookings/list-bookings.component';

const routes: Routes = [
  {path: 'bookings', component: ListBookingsComponent},
  {path: 'add-booking', component: AddBookingComponent},
  {path: 'edit-booking/:id', component: AddBookingComponent},
  {path: '', redirectTo: '/bookings', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
