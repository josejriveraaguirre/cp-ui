import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListBookingsComponent } from './components/list-bookings/list-bookings.component';
import { AddBookingComponent } from './components/add-booking/add-booking.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBookingsComponent,
    AddBookingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
